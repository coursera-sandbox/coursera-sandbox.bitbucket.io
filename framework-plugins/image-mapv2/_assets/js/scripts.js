
/** Code Snippet JavaScript **/
/* Needs to go at top of script file so copies unaltered code */
$.each($('.component-sample'), (function () {
   var componentCode = $(this).html();
   var codeReplace = {
      '<':'&lt;',
      '>':'&gt;',
      '  ':''
   };
   componentCode = componentCode.replace(/<|>|  /gi, (function(matched){
      return codeReplace[matched];
    }));
    //var componentCode = componentCode.replace(/(?:\r\n|\r|\n)/gi, '');
   $(this).append('<pre class="code-sample copy-this card bg-light"><code>' + componentCode + '</code></pre><p><button class="btn copy-btn" data-toggle="tooltip" data-placement="top" title="Copy Code To Clipboard">Copy Code</button></p>');
}));


/** Copy Button JavaScript **/
$(".copy-btn").on('click', (function () {
   var copyText = $(this).parent().prev('.copy-this');
   /* copyToClipBoard(copyText[0].textContent); */
   var copyTextString = String(copyText[0].textContent);
   var tempTextArea = document.createElement('textarea');
   tempTextArea.value = copyTextString;
   document.body.appendChild(tempTextArea);
   tempTextArea.select();
   document.execCommand('copy');
   document.body.removeChild(tempTextArea);
   $(this).html('Code Copied');
}));

/** Accordions JavaScript **/

/** Accordions - dynamically add id **/
$.each($(".accordion"), (function (index) {
   $(this).attr("id", "accordion_" + parseInt(index + 1));
}));

/** Accordions - dynamically add interaction **/
$.each($(".accordion > .card"), (function (index, value) {
   var num = index + 1;
   $(value).children(".card-header").attr("id", "heading_acc_" + num);
   $(value).find(".card-header > .card-title").wrapInner( "<button  class=\"btn btn-link\" type=\"button\" data-toggle=\"collapse\" aria-expanded=\"false\"></button>");
   $(value).find(".card-header > .card-title > button").attr({
      "data-target": "#collapse_acc_" + num,
      "aria-controls": "collapse_acc_" + num
   });
   $(value).children(".collapse").attr({
      id: "collapse_acc_" + num,
      "aria-labelledby": "heading_acc_" + num
   });
}));

/* button to open & close all accordion slides. */
$('.expandall').on('click', (function () {
   var nextAccordion = $(this).parent().next('.accordion');
   /* in case button is not in p tag */
   if ($(this).next('.accordion').length > 0) {
      nextAccordion = $(this).next('.accordion');
   }
   var nextAccId = '#' + nextAccordion.attr('id');
   if ($(nextAccId).hasClass("show-all")) {
      $(nextAccId + ' .collapse.show').collapse('hide');
      $(nextAccId).removeClass("show-all");
      $(this).attr("aria-expanded","false");
      $(this).text("Open All Panels");
   } else {
      $(nextAccId + ' .collapse:not(".show")').collapse('show');
      $(nextAccId).addClass("show-all");
      $(this).attr("aria-expanded","true");
      $(this).text("Close All Panels");
   }
}));

/** Background Image JavaScript **/
/* Get all the elements with the class bg-img-wrapper on page */
var bgImgWrapper = document.getElementsByClassName("bg-img-wrapper");
/* Cycle through the elements we want to have a background image */
for(var bgImgIndex = 0; bgImgIndex < bgImgWrapper.length; bgImgIndex++)
{
   /* Declare variable bgImgId */
   var bgImgWrapperId = "bg-img-wrapper-" + parseInt(bgImgIndex);
   /* Add bgImgWrapperId as unique ID to each element with class .bg-img-wrapper */
   document.querySelectorAll('.bg-img-wrapper')[bgImgIndex].setAttribute("id", bgImgWrapperId);
   /* Get the background image from the source of the first child image */
   var bgImg = document.getElementById(bgImgWrapperId).getElementsByTagName('img')[0].src;
   /* Set the background image on each element with class .bg-img-wrapper */
   document.querySelectorAll('.bg-img-wrapper')[bgImgIndex].setAttribute("style", 'background-image: url(' + bgImg + ');');
}

/** Buttons JavaScript **/

/** Click and Reveal JavaScript **/
$.each($(".card-reveal"), (function (index, value) {
   var num = index + 1;
   $(value).find(".card-body > .card-text > .btn-reveal").attr({
      "data-target": "#c2r" + num,
      "id": "c2s" + num,
   });
   $(value).find(".card-body > .card-text > .collapse").attr({
      "id": "c2r" + num,
      "aria-labelledby": "c2s" + num
   });
}));


/** Click and Reveal: Inline Question w/ Hero Headline JavaScript **/
$.each($(".overlay-reveal"), (function (index, value) {
   var num = index + 1;
   $(value).find(".btn").attr({
      "data-target": "#collapseExample" + num,
      "aria-controls": "collapseExample" + num,
   });
   $(value).find(".collapse").attr({
      "id": "collapseExample" + num
   });
}));

/** Click and Reveal: Table JavaScript **/
$.each($(".table-reveal tr"), (function (index, value) {
   var num = index + 1;
   $(value).find(".btn-reveal").attr({
      "data-target": "#tc2r" + num,
      "id": "tc2s" + num
   });
   $(value).find(".collapse").attr({
      "id": "tc2r" + num,
      "aria-labelledby": "tc2s" + num
   });
}));

/* Disable button onclick */
$('.btn-reveal').on('click', (function () {
   var $_this = $(this);
   setTimeout((function () {
       $_this.attr('disabled', true);
   }), 500);
}));




/** Flipcards JavaScript **/
$('.flip-card-content').on('click', (function () {
   $(this).toggleClass('apply-flip');
}));
/** Trigger Click on Focus + Enter  **/
$('.flip-card-content').keydown((function (e) {
   var keyCode = (e.keyCode ? e.keyCode : e.which);
   if (keyCode === 13) {
      $(e.target).trigger('click');
   }
}));

/** Icons JavaScript **/

/** Links JavaScript **/
$((function () {
   $('[data-toggle="tooltip"]').tooltip();
 }));
 
 /* Check for links in document */ 
 var links = document.querySelectorAll("a");
 /* Create index for download links unique id*/
 var downloadIndex = 0;
 /* Create index for new window links unique id*/
 var newWindowIndex = 0;
 /* Check links on page */
 for(var linkIndex = 0; linkIndex < links.length; linkIndex++)
 {
    /* Creating a span to wrap the screen-reader text */ 
    var srTxtWrapper = document.createElement("span");
    /* Add class .sr-only to screen-reader span */
    srTxtWrapper.classList.add("sr-only");
   
 if (links[linkIndex].classList.contains("download")) {
   /* Add download attribute */
   links[linkIndex].setAttribute("download", "");
   /* Add unique id to download link */
   links[linkIndex].setAttribute("id", "download-file-" + downloadIndex);
   /* Add title attribute saying download file */
   links[linkIndex].setAttribute("title", "download file");
   /* Add data-toggle tooltip data attribute */
   links[linkIndex].setAttribute("data-toggle", "tooltip");
    /* Creating the screen-reader text */ 
   var srTxt = document.createTextNode("(this link downloads a file)");
   /* Adding the screen-reader text to the span*/ 
   srTxtWrapper.appendChild(srTxt); 
   links[linkIndex].appendChild(srTxtWrapper);
   /* Increase downloadIndex by one for next download link */
   downloadIndex++;
 }
 else if (links[linkIndex].classList.contains("new-window")) {
   /* Add target _blank attribute for link to open in new window */
   links[linkIndex].setAttribute("target", "_blank");
   /* Add unique id to new window link */
   links[linkIndex].setAttribute("id", "new-window" + newWindowIndex);
   /* Add title attribute saying link opens in new window */
   links[linkIndex].setAttribute("data-original-title", "opens in new window/tab");
   /* Add data-toggle tooltip data attribute */
   links[linkIndex].setAttribute("data-toggle", "tooltip");
   /* Add rel="noopener" for security - see https://developers.google.com/web/tools/lighthouse/audits/noopener and https://mathiasbynens.github.io/rel-noopener/ */
   links[linkIndex].setAttribute("rel", "noopener");
   /* Creating the screen-reader text */ 
   var srTxt = document.createTextNode("(this link opens in a new window/tab)");
   /* Adding the screen-reader text to the span*/ 
   srTxtWrapper.appendChild(srTxt); 
   links[linkIndex].appendChild(srTxtWrapper);
   newWindowIndex++;
 }
 }


/** Modals JavaScript **/
$.each($(".modal-set"), (function (index, value) {
   var num = index + 1;
   $(value).find(".btn").attr({
      "data-target": "#exampleModal" + num
   });
   $(value).find(".modal").attr({
      "id": "exampleModal" + num,
      "aria-labelledby": "exampleModalLabel" + num
   });
   $(value).find(".modal-title").attr({
      "id": "exampleModalLabel" + num
   });
}));

if (window.self !== window.top) {
   $('[data-toggle="modal"]').click((function () {
      /* Gets the top offset position of the button triggering modal */
      var buttonTopOffset = $(this).offset().top;
     $('.modal').on('show.bs.modal', (function () {
        /* Adds he top offset position of the button as padding-top to the modal  */
      $(this).css("padding-top", buttonTopOffset);
     }));
   }));
}


/** Popovers JavaScript **/
$((function () {
   $('[data-toggle="popover-top"]').popover({
     html: true
     
     
 
   });
 
   $('[data-toggle="popover-bottom"]').popover({
     html: true
    
     
  
   });
 
   $('[data-toggle="popover-left"]').popover({
     html: true
     
     
  
   });
 
   $('[data-toggle="popover-right"]').popover({
      html: true
    
     
  
   });
 
 //   $('body').on('click', function (e) {
 //     $('[data-toggle="popover"]').each(function () {
 //         //the 'is' for buttons that trigger popups
 //         //the 'has' for icons within a button that triggers a popup
 //         if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
 //             $(this).popover('hide');
 //         }
 //     });
 // });
 }));

/** Quick Assesments JavaScript **/
/* Quick Assessment - Multiple Choice */
/* Adding attributes to Quick Assessment - Multiple Choice */ 
$.each($(".quick-assess-mc"), (function (index, value) {
   var num = index + 1;
   $(value).find(".btn-quick-assess")
   .attr({
      "data-target": "#MC-QuizFeedback" + num,
      "aria-controls": "MC-QuizFeedback" + num,
      "id": "MC-QuizButton" + num
   });
   $(value).find(".collapse").attr({
      "id": "MC-QuizFeedback" + num
   });
   /* Enable Button to Check Answer */
   var MCQuizButtonID = "#MC-QuizButton" + num;
   $(value).find("input").on('click', (function () {
      $(MCQuizButtonID).removeAttr("disabled");
   }));
   /* When displaying answer - show answer symbols and disable button and radio buttons */
   $(MCQuizButtonID).click((function () {
      $(value).find(".ans-symbol").removeClass("invisible");
      $(value).find('.collapse').on('shown.bs.collapse', (function () {
         $(value).find(".btn-quick-assess, input").prop('disabled', true);
      }));
   }));
}));

$.each($(".quick-assess-mc .form-check"), (function (index, value) {
   var num = index + 1;
   $(value).find("input").attr({
      "id": "MC-ans" + num,
      "value": "MC-ans" + num
   });
   $(value).find("label").attr({
      "for": "MC-ans" + num
   });
}));

/* Quick Assessment - All That Apply (Multi-select) */
/* Adding attributes to Quick Assessment - All That Apply (Multi-select)  */ 
$.each($(".quick-assess-ms"), (function (index, value) {
   var num = index + 1;
   $(value).find(".btn-quick-assess")
   .attr({
      "data-target": "#MS-QuizFeedback" + num,
      "aria-controls": "MS-QuizFeedback" + num,
      "id": "MS-QuizButton" + num
   });
   $(value).find(".collapse").attr({
      "id": "MS-QuizFeedback" + num
   });
   /* Enable Button to Check Answer */
   var MSQuizButtonID = "#MS-QuizButton" + num;
   $(value).find("input").on('click', (function () {
      $(MSQuizButtonID).removeAttr("disabled");
   }));
   /* When displaying answer - show answer symbols and disable button and radio buttons */
   $(MSQuizButtonID).click((function () {
      $(value).find(".ans-symbol").removeClass("invisible");
      $(value).find('.collapse').on('shown.bs.collapse', (function () {
         $(value).find(".btn-quick-assess, input").prop('disabled', true);
      }));
   }));
}));

$.each($(".quick-assess-ms .form-check"), (function (index, value) {
   var num = index + 1;
   $(value).find("input").attr({
      "id": "MS-ans" + num,
      "value": "MS-ans" + num
   });
   $(value).find("label").attr({
      "for": "MS-ans" + num
   });
}));

/* Quick Assessment - Short Answers (Fill in the Blank) */
$.each($(".quick-assess-sa"), (function (index, value) {
   var num = index + 1;
   /*  Enable Button to Check Answer */
   $(value).find('input[type=text]').keyup((function () {
      var noInput = false;
      $(value).find('input[type=text]').each((function () {
         if ($(this).val() == '') {
            noInput = true;
         }
      }));
      if (noInput) {
         $(value).find(".btn-quick-assess").prop('disabled', true);
      } else {
         $(value).find(".btn-quick-assess").removeAttr("disabled");
      }
   }));

   $(value).find(".btn-quick-assess")
   .attr({
      "data-target": "#FIB-QuizFeedback" + num,
      "aria-controls": "FIB-QuizFeedback" + num,
      "id": "FIB-QuizButton" + num
   });
   $(value).find(".collapse").attr({
      "id": "FIB-QuizFeedback" + num
   });
   /* When displaying answer disable buttons and form fields */
   var FIBButtonID = "#FIB-QuizButton" + num;
   $(FIBButtonID).click((function () { 
      $(value).find('.collapse').on('shown.bs.collapse', (function () {
         $(value).find(".btn-quick-assess, input").prop('disabled', true);
      }));
   }));
}));
/* Adding attributes to Quick Assessment - Short Answers (Fill in the Blank)  */ 
$.each($(".quick-assess-sa .shortanswer"), (function (index, value) {
   var num = index + 1;
   $(value).find("label").attr({
      "for": "FIB-answer" + num
   });
   $(value).find("input").attr({
      "id": "FIB-answer" + num
   });
  
}));

/* Quick Assessment - Drop Down */
$.each($(".quick-assess-dd"), (function (index, value) {
   var num = index + 1;
   /* Enable Button to Check Answer  */
   $(value).find('select').change((function () {
      var noInput = false;
      $(value).find('select').each((function () {
         if ($(this).val() == null) {
            noInput = true;
         }
      }));
      if (noInput) {
         $(value).find(".btn-quick-assess").prop('disabled', true);
      } else {
         $(value).find(".btn-quick-assess").removeAttr("disabled");
      }
   }));
   $(value).find(".btn-quick-assess")
   .attr({
      "data-target": "#DD-QuizFeedback" + num,
      "aria-controls": "DD-QuizFeedback" + num,
      "id": "DD-QuizButton" + num
   });
   $(value).find(".collapse").attr({
      "id": "DD-QuizFeedback" + num
   });
   /* When displaying answer disable buttons and form fields */
   var DDButtonID = "#DD-QuizButton" + num;
   $(DDButtonID).click((function () {
      $(value).find('.collapse').on('shown.bs.collapse', (function () {
         $(value).find(".btn-quick-assess, select").prop('disabled', true);
      }));
   }));
}));
/* Adding attributes to Quick Assessment - Drop Down  */ 
$.each($(".quick-assess-dd label"), (function (index) {
   $(this).attr({
      "for": "DD-answer" + parseInt(index + 1)
   });
}));
$.each($(".quick-assess-dd select"), (function (index) {
   $(this).attr({
      "id": "DD-answer" + parseInt(index + 1),
      "name": "select" + parseInt(index + 1)
   });
}));

/** Tabs JavaScript **/
/** Tabs - dynamically add interaction **/
/* .list-group add ID */
$.each($(".list-group"), (function (index) {
   $(this).attr("id", "list-tab_" + parseInt(index + 1));
}));
/* .list-group-item add attributes */
$.each($(".list-group > .list-group-item"), (function (index, value) {
   var num = index + 1;
   $(value).attr({
      "id": "list-" + num + "-list",
      "href": "#list-" + num,
      "aria-controls": "list-" + num
   });
}));
/* .tab-content add ID */
$.each($(".tab-content"), (function (index) {
   $(this).attr("id", "nav-tabContent_" + parseInt(index + 1));
}));
/* .tab-pane add attributes */
$.each($(".tab-content > .tab-pane"), (function (index, value) {
   var num = index + 1;
   $(value).attr({
      "id": "list-" + num,
      "aria-labelledby": "list-" + num + "-list"
   });
}));

 /** Tooltip JavaScript **/
 $((function () {
   $('[data-toggle="tooltip"]').tooltip();
 }));

 /** Video JavaScript **/

 /** WheelNav JavaScript **/
 $(".btn-reveal1").click((function () {
   $(".collapse").collapse('hide');
}));

